<?php
/*
 * Plugin Name: Generate text
 * Plugin URI: https://devqanh.info
 * Version:           1.0
 * Description: Generate text shortcode [generate-text]

 */
define('qaplugin', plugin_dir_url(__FILE__));

add_shortcode('generate-text', function () {
    ob_start();
    ?>
    <div id="form-submit-qa">
        <div class="form-group">
            <input type="text" name="text" id="text" placeholder="»—- Nhập tên của bạn —-«" class="form-control"
                   required="required">
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <select name="sLeft" id="sLeft" class="form-control">
                        <option value="">Trái</option>
                        <option value="◥ὦɧ◤">◥ὦɧ◤</option>
                        <option value="ᴳᵒᵈ乡">ᴳᵒᵈ乡</option>
                        <option value="ᵈʳᵉᵃᵐ乡">ᵈʳᵉᵃᵐ乡</option>
                        <option value="๖ۣۜҨž乡">Ҩž乡</option>
                        <option value="๖ۣۜƝƘ☆">๖ۣۜƝƘ☆</option>
                        <option value="ƒさ→">ƒさ→</option>
                        <option value="ᎮᏁ丶">ᎮᏁ丶</option>
                        <option value="ɱ√ρ︵">ɱ√ρ︵</option>
                        <option value="๖ACE✪">๖ACE✪</option>
                        <option value="ღᏠᎮღ">ღᏠᎮღ</option>
                        <option value="꧁">꧁</option>
                        <option value="࿐">࿐</option>
                        <option value="ঔ">ঔ</option>
                        <option value="ঌ">ঌ</option>
                        <option value="♚">♚</option>
                        <option value="♕">♕</option>
                        <option value="✿">✿</option>
                        <option value="❄">❄</option>
                        <option value="❤">❤</option>
                        <option value="ツ">ツ</option>
                        <option value="☆">☆</option>
                        <option value="➻❥">➻❥</option>
                        <option value="❖︵">❖︵</option>
                        <option value="✎﹏">✎﹏</option>
                        <option value="ミ★">ミ★</option>
                        <option value="๖">๖</option>
                        <option value="▲">▲</option>
                        <option value="◇">◇</option>
                        <option value="◆">◆</option>
                        <option value="☠">☠</option>
                        <option value="❣">❣</option>
                        <option value="➻">➻</option>
                        <option value="✎">✎</option>
                        <option value="☓">☓</option>
                        <option value="ღ">ღ</option>
                        <option value="☂">☂</option>
                        <option value="❦">❦</option>
                        <option value="☗">☗</option>
                        <option value="﹏">﹏</option>
                        <option value="❖">❖</option>
                        <option value="۶">۶</option>
                        <option value="✚">✚</option>
                        <option value="ʚɞ">ʚɞ</option>
                        <option value="☞╯">☞╯</option>
                        <option value="✔">✔</option>
                        <option value="✾">✾</option>
                        <option value="♥">♥</option>
                        <option value="♪">♪</option>
                        <option value="●">●</option>
                        <option value="■">■</option>
                        <option value="✌">✌</option>
                        <option value="☝">☝</option>
                        <option value="☥">☥</option>
                        <option value="♜">♜</option>
                        <option value="✟">✟</option>
                        <option value="❖">❖</option>
                        <option value="๖ۣۜ">๖ۣۜ</option>
                        <option value="✦">✦</option>
                        <option value="✼">✼</option>
                        <option value="⊹⊱">⊹⊱</option>
                        <option value="⊰⊹">⊰⊹</option>
                        <option value="╰❥">╰❥</option>
                        <option value="↭">↭</option>
                        <option value="❛❜">❛❜</option>
                        <option value="ᴾᴿᴼシ">ᴾᴿᴼシ</option>
                        <option value="ᵛᶰシ">ᵛᶰシ</option>
                        <option value="๖²⁴ʱ">๖²⁴ʱ</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select name="sCenter" id="sCenter" class="form-control">
                        <option value="">Giữa</option>
                        <option value="•">•</option>
                        <option value="ঔ">ঔ</option>
                        <option value="ঌ">ঌ</option>
                        <option value="♚">♚</option>
                        <option value="♕">♕</option>
                        <option value="✿">✿</option>
                        <option value="❄">❄</option>
                        <option value="❤">❤</option>
                        <option value="ツ">ツ</option>
                        <option value="☆">☆</option>
                        <option value="๖">๖</option>
                        <option value="◇">◇</option>
                        <option value="◆">◆</option>
                        <option value="☠">☠</option>
                        <option value="❣">❣</option>
                        <option value="ღ">ღ</option>
                        <option value="❦">❦</option>
                        <option value="﹏">﹏</option>
                        <option value="❖">❖</option>
                        <option value="۶">۶</option>
                        <option value="✾">✾</option>
                        <option value="♥">♥</option>
                        <option value="●">●</option>
                        <option value="❖">❖</option>
                        <option value="๖ۣۜ">๖ۣۜ</option>
                        <option value="✦">✦</option>
                        <option value="✼">✼</option>
                        <option value="↭">↭</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select name="sRight" id="sRight" class="form-control">
                        <option value="">Phải</option>
                        <option value="꧂">꧂</option>
                        <option value="࿐">࿐</option>
                        <option value="♚">♚</option>
                        <option value="♕">♕</option>
                        <option value="✿">✿</option>
                        <option value="❄">❄</option>
                        <option value="❤">❤</option>
                        <option value="ツ">ツ</option>
                        <option value="☆">☆</option>
                        <option value="‿✶">‿✶</option>
                        <option value="︵✰">︵✰</option>
                        <option value="﹏✍">﹏✍</option>
                        <option value="︵❣">︵❣</option>
                        <option value="﹏❣">﹏❣</option>
                        <option value="★彡">★彡</option>
                        <option value="▲">▲</option>
                        <option value="◇">◇</option>
                        <option value="◆">◆</option>
                        <option value="☠">☠</option>
                        <option value="❣">❣</option>
                        <option value="➻">➻</option>
                        <option value="✎">✎</option>
                        <option value="☓">☓</option>
                        <option value="ღ">ღ</option>
                        <option value="☂">☂</option>
                        <option value="❦">❦</option>
                        <option value="☗">☗</option>
                        <option value="﹏">﹏</option>
                        <option value="❖">❖</option>
                        <option value="۶">۶</option>
                        <option value="✚">✚</option>
                        <option value="ʚɞ">ʚɞ</option>
                        <option value="╰☜">╰☜</option>
                        <option value="✔">✔</option>
                        <option value="✾">✾</option>
                        <option value="♥">♥</option>
                        <option value="♪">♪</option>
                        <option value="●">●</option>
                        <option value="■">■</option>
                        <option value="✌">✌</option>
                        <option value="☝">☝</option>
                        <option value="☥">☥</option>
                        <option value="♜">♜</option>
                        <option value="✟">✟</option>
                        <option value="❖">❖</option>
                        <option value="๖ۣۜ">๖ۣۜ</option>
                        <option value="✦">✦</option>
                        <option value="✼">✼</option>
                        <option value="⊹⊱">⊹⊱</option>
                        <option value="⊰⊹">⊰⊹</option>
                        <option value="╰❥">╰❥</option>
                        <option value="↭">↭</option>
                        <option value="❛❜">❛❜</option>
                        <option value="₆₇₈₉">₆₇₈₉</option>
                        <option value="︵❻❾">︵❻❾</option>
                        <option value="︵⁹⁰">︵⁹⁰</option>
                        <option value="︵⁹¹">︵⁹¹</option>
                        <option value="︵⁹²">︵⁹²</option>
                        <option value="︵⁹³">︵⁹³</option>
                        <option value="︵⁹⁴">︵⁹⁴</option>
                        <option value="︵⁹⁵">︵⁹⁵</option>
                        <option value="︵⁹⁶">︵⁹⁶</option>
                        <option value="︵⁹⁷">︵⁹⁷</option>
                        <option value="︵⁹⁸">︵⁹⁸</option>
                        <option value="︵⁹⁹">︵⁹⁹</option>
                        <option value="︵²⁰⁰⁰">︵²⁰⁰⁰</option>
                        <option value="︵²⁰⁰¹">︵²⁰⁰¹</option>
                        <option value="︵²⁰⁰²">︵²⁰⁰²</option>
                        <option value="︵²⁰⁰⁴">︵²⁰⁰⁴</option>
                        <option value="︵²⁰⁰⁵">︵²⁰⁰⁵</option>
                        <option value="₠">₠</option>
                        <option value="™">™</option>
                        <option value="℠">℠</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-info btn-block" id="btnGenerator">Tạo chữ</button>
        </div>
        <div class="loading-qa">
            <img src="<?php echo qaplugin; ?>/assets/img/comet-spinner.svg"/>
        </div>
        <div id="result"></div>
    </div>
    <style>
        #form-submit-qa *{font-family:Roboto,sans-serif}#form-submit-qa .loading-qa{display:none}#form-submit-qa .btn{display:inline-block;padding:6px 12px;margin-bottom:0;font-size:14px;font-weight:400;line-height:1.42857143;text-align:center;white-space:nowrap;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px}#form-submit-qa .btn-info{color:#fff;background-color:#0084ff}#form-submit-qa .btn-block{display:block;width:100%}#form-submit-qa .form-control{width:100%;height:34px;padding:6px 12px;font-size:14px;color:#555;background-color:#fff;background-image:none;border:1px solid #ccc;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);box-shadow:inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s}#form-submit-qa .form-group{margin-bottom:15px}#form-submit-qa .row{margin-right:-15px;margin-left:-15px}#form-submit-qa .col-md-4{position:relative;min-height:1px;padding-left:15px;padding-right:15px}@media (min-width:992px){#form-submit-qa .col-md-4{float:left}#form-submit-qa .col-md-4{width:33.33333333%}}#form-submit-qa .input-group{display:table;border-collapse:separate}#form-submit-qa .loading-qa{text-align:center}#form-submit-qa .input-group .form-control,#form-submit-qa .input-group-addon,#form-submit-qa .input-group-btn{display:table-cell}#form-submit-qa .input-group-addon,#form-submit-qa .input-group-btn{width:1%;white-space:nowrap;vertical-align:middle}#form-submit-qa .input-group-addon{padding:6px 12px;font-size:14px;font-weight:400;line-height:1;color:#555;background-color:#eee;border:1px solid #ccc}#form-submit-qa .input-group-addon:first-child{border-right:0}#form-submit-qa .input-group-addon:last-child{border-left:0}#form-submit-qa .input-group-btn{font-size:0;white-space:nowrap}#form-submit-qa .loading-qa img { max-width: 80px;}
    </style>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script>
        function copyToClipboardText(e) {
            copyToClipboard(e.parentElement.parentElement.children[1]), e.classList.add("btn-info"), e.innerHTML = "Copied"
        }

        function copyToClipboard(e) {
            var t;
            target = e, target.focus(), target.setSelectionRange(0, target.value.length);
            try {
                t = document.execCommand("copy")
            } catch (e) {
                t = !1
            }
            return t
        };
        jQuery(document).ready(function ($) {

            $('#form-submit-qa #btnGenerator').click(function () {
                var text = $('#form-submit-qa #text').val();

                var sLeft = $('#form-submit-qa #sLeft').val();
                var sRight = $('#form-submit-qa #sRight').val();
                var sCenter = $('#form-submit-qa #sCenter').val();
                if (text == '') {
                    alert('Vui Lòng nhập text !!!');
                    return;
                }
                $('#form-submit-qa .loading-qa').show();

                $('#form-submit-qa #result').fadeOut();
                $.ajax({
                    type: "post",
                    dataType: "html",
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        action: "generationqa",
                        text: text,
                        sLeft: sLeft,
                        sRight: sRight,
                        sCenter: sCenter
                    },
                    success: function (response) {
                        $('#form-submit-qa #result').html(response);
                        $('#form-submit-qa .loading-qa').hide();
                    }
                });
                setTimeout(function () {
                    $('#form-submit-qa .loading-qa').hide();
                }, 1500);
                return false;
            });
        })
    </script>
    <?php
    return ob_get_clean();
});
add_action('wp_ajax_generationqa', 'generationqa_init');
add_action('wp_ajax_nopriv_generationqa', 'generationqa_init');
function generationqa_init()
{


    $text = (isset($_POST['text'])) ? esc_attr($_POST['text']) : '';
    $sLeft = (isset($_POST['sLeft'])) ? esc_attr($_POST['sLeft']) : '';
    $sRight = (isset($_POST['sRight'])) ? esc_attr($_POST['sRight']) : '';
    $sCenter = (isset($_POST['sCenter'])) ? esc_attr($_POST['sCenter']) : '';

    echo generationqa_process($text, $sLeft, $sRight, $sCenter);

    die();
}

function generationqa_process($text, $sleft, $sright, $scenter)
{

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://kitudacbiet.com/ajax.php",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "text=$text&sLeft=$sleft&sRight=$sright&sCenter=$scenter",
        CURLOPT_HTTPHEADER => array(
            "accept-language: vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5",
            "authority: kitudacbiet.com",
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "origin: https://kitudacbiet.com",
            "referer: https://kitudacbiet.com/?fbclid=IwAR35fGIqv52d-XajCVo0Zd66DoFX7AKvtgS73QFfkhGNliOY94gRXE_-6QY",
            "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } else {
        return $response;
    }
}
